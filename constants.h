#ifndef CONSTANTS_H
#define CONSTANTS_H
#include <QString>

#define WINDOWS
#ifdef LUNAR_DEBUG
#define SOFTWARE_VERSION QString("0.9 - Debug")
#else
#define SOFTWARE_VERSION QString("0.9")
#endif
#define CONFIGURATION_FILE "data/config.ini"
#define CACHE_PATH "cache/"

#endif // CONSTANTS_H
