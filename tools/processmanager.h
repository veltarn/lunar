#ifndef PROCESSMANAGER_H
#define PROCESSMANAGER_H

#include <iostream>
#include <QObject>
#include <QModelIndex>
#include <QMessageBox>
#include <QMap>
#include <QFileInfo>
#include <QString>
#include "../games/db/games.h"
#include "process.h"
#include "singleton.h"
#include "constants.h"

class ProcessManager : public Singleton<ProcessManager>
{
   Q_OBJECT
public:

    void startGame(QModelIndex indexGame);
    bool processExists(QModelIndex mi); //Search if the process associated to "mi" exists
    QMap<Process*, QModelIndex> &getList();

public slots:
    void onProcessFinished(int exitCode, QProcess::ExitStatus status);
private:
    QMap<Process*, QModelIndex> m_processesList;
    Games m_game;
};

#endif // PROCESSMANAGER_H
