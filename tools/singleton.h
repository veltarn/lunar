#ifndef SINGLETON_H
#define SINGLETON_H

#include <iostream>
#include <QObject>
template <typename T>
class Singleton : public QObject
{
protected:
    Singleton(){ }
    ~Singleton(){}
public:
    static T *getInstance()
    {
        if(m_singleton == NULL)
        {
            m_singleton = new T;
        }
        return (static_cast<T*> (m_singleton));
    }

    static void kill()
    {
        if(m_singleton != NULL)
        {
            delete m_singleton;
            m_singleton = NULL;
        }
    }

private:
    //Instance unique
    static T *m_singleton;
};

template <typename T>
T *Singleton<T>::m_singleton = NULL;

#endif // SINGLETON_H
