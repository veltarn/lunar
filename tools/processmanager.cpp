#include "processmanager.h"

using namespace std;


void ProcessManager::startGame(QModelIndex indexGame)
{
    Process *process = new Process;
    QModelIndex indexPath = indexGame.sibling(indexGame.row(), 2);

    QString url = indexPath.data().toString();
    QFileInfo fileInfo(url);
#ifdef WINDOWS
    //Replacing slashes by anti slashes on windows
    url.replace("/", "\\");
    url = "\"" + url + "\"";
#endif
    //Starting the game only if the process doesnt exists
    if(!processExists(indexGame))
    {
        m_processesList[process] = indexGame;

        connect(process, SIGNAL(finished(int,QProcess::ExitStatus)), this, SLOT(onProcessFinished(int,QProcess::ExitStatus)));

        process->setWorkingDirectory(fileInfo.path());
        process->start(url, QStringList());
    }
    else
    {
        QMessageBox::warning(0, tr("Warning"), QString("<b>" + indexGame.data().toString() + "</b> " + tr("is already running")));
    }
}

void ProcessManager::onProcessFinished(int exitCode, Process::ExitStatus status)
{
    Q_UNUSED(exitCode);
    Q_UNUSED(status);

    Process *p = dynamic_cast<Process*>(sender());

    QModelIndex mi = m_processesList[p];
    QModelIndex id = mi.sibling(mi.row(), 1);

    int playDuration = p->getPlayedTime();

    m_game.updatePlayTime(id.data().toInt(), playDuration);
    m_game.syncSaves(id.data().toInt());

    m_processesList.remove(p);

    delete p;
    p = NULL;
}

bool ProcessManager::processExists(QModelIndex mi)
{
    for(QMap<Process*, QModelIndex>::iterator it = m_processesList.begin(); it != m_processesList.end(); ++it)
    {
        if(*it == mi)
        {
            return true;
        }
    }

    return false;
}

QMap<Process*, QModelIndex> &ProcessManager::getList()
{
    return m_processesList;
}
