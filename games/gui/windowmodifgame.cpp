#include "windowmodifgame.h"

using namespace std;

WindowModifGame::WindowModifGame(QWidget *parent, int gameId) :
    QDialog(parent),
    m_lastPositionExeWindow(""),
    m_lastPositionSaveWindow(""),
    m_gameId(gameId),
    m_currentRemoteGameId(0)
{
    this->setMinimumSize(500, 300);
    this->resize(500, 300);
    m_api = ApiSelector::getUsedApi();

    this->buildGui();
    this->initEvents();

    //If a game have to be edited
    if(m_gameId > 0)
    {
        this->loadGameData();
    }
}

WindowModifGame::~WindowModifGame()
{
}

void WindowModifGame::buildGui()
{
    m_mainLayout                = new QVBoxLayout;
    m_buttonsLayout             = new QHBoxLayout;
    m_formLayout                = new QFormLayout;
    m_savesFormLayout           = new QFormLayout;
    m_exePathLayout             = new QHBoxLayout;
    m_uninstallPathLayout       = new QHBoxLayout;
    m_savePathLayout            = new QHBoxLayout;
    m_gameNameLayout            = new QHBoxLayout;

    m_lineGameName              = new QLineEdit(this);
    m_lineExePath               = new QLineEdit(this);
    m_lineUninstallPath         = new QLineEdit(this);
    m_lineSavePath              = new QLineEdit(this);

    m_gameNameInfoLabel         = new QLabel(this);

    m_boxSaves                  = new QGroupBox(tr("Saves"), this);

    m_btnExePathBrowse          = new Button(tr("Browse"), this);
    m_btnValidate               = new Button(tr("Validate"), this);
    m_btnSavePathBrowse         = new Button(tr("Browse"), this);
    m_btnUninstallPathBrowse    = new Button(tr("Browse"), this);
    m_btnCancel                 = new Button(tr("Cancel"), this);

    if(m_api != NULL && m_api->isOnlineApi())
    {
        m_btnSearchGame         = new Button(tr("Search"), this);
        m_chkDeleteGameLink     = new QCheckBox(tr("Delete current link"), this);
    }

    m_gameNameInfoLabel->hide();
    m_lineExePath->setEnabled(false);
    m_lineSavePath->setEnabled(false);    
    m_lineUninstallPath->setEnabled(false);

    m_boxSaves->setCheckable(true);
    m_boxSaves->setChecked(false);
    m_btnSavePathBrowse->setEnabled(false);
    if(m_api != NULL && m_api->isOnlineApi())
    {
        m_btnSearchGame->setEnabled(false);

        //We hide the checkbox if the user try to create a new game
        if(m_gameId == 0)
        {
            m_chkDeleteGameLink->setEnabled(false);
            m_chkDeleteGameLink->hide();
        }
    }

    //Layouting
    //Game name layout (textEdit and if the current api is online, search button)
    m_gameNameLayout->addWidget(m_lineGameName, 3);
    if(m_api != NULL && m_api->isOnlineApi())
    {
        m_gameNameLayout->addWidget(m_btnSearchGame, 1);
    }

    //Création exe file search layout (which is constituted of 2 widgets)
    m_exePathLayout->addWidget(m_lineExePath, 3);
    m_exePathLayout->addWidget(m_btnExePathBrowse, 1);

    m_uninstallPathLayout->addWidget(m_lineUninstallPath, 3);
    m_uninstallPathLayout->addWidget(m_btnUninstallPathBrowse, 1);

    //Création save folder search layout (which is constituted of 2 widgets)
    m_savePathLayout->addWidget(m_lineSavePath, 3);
    m_savePathLayout->addWidget(m_btnSavePathBrowse, 1);

    //Putting QLineEdit of game name and exe layout into formLayout
    m_formLayout->addRow(tr("Game name* "), m_gameNameLayout);
    m_formLayout->addWidget(m_gameNameInfoLabel);
    if(m_api && m_api->isOnlineApi())
        m_formLayout->addWidget(m_chkDeleteGameLink);
    m_formLayout->addRow(tr("Executable path* "), m_exePathLayout);

    m_formLayout->addRow(tr("Uninstall path (if exists)"), m_uninstallPathLayout);

    //Adding save path layout to a row of save form layout
    m_savesFormLayout->addRow(tr("Game save folder"), m_savePathLayout);

    // Setting m_savesFormLayout as layout of the box m_boxSaves
    m_boxSaves->setLayout(m_savesFormLayout);

    m_buttonsLayout->addWidget(m_btnCancel);
    m_buttonsLayout->addWidget(m_btnValidate);

    m_mainLayout->addLayout(m_formLayout, 1);
    m_mainLayout->addWidget(m_boxSaves, 3);
    m_mainLayout->addLayout(m_buttonsLayout);

    this->setLayout(m_mainLayout);
}

void WindowModifGame::initEvents()
{
    connect(m_btnCancel, SIGNAL(clicked()), this, SLOT(accept()));
    connect(m_btnExePathBrowse, SIGNAL(clicked()), this, SLOT(openExeFileSelector()));
    connect(m_boxSaves, SIGNAL(clicked(bool)), this, SLOT(enableSaveSyncBox(bool)));
    connect(m_btnUninstallPathBrowse, SIGNAL(clicked()), this, SLOT(openUninstallFileSelector()));
    connect(m_btnSavePathBrowse, SIGNAL(clicked()), this, SLOT(openSavePathSelector()));
    connect(m_btnValidate, SIGNAL(clicked()), this, SLOT(validateForm()));

    if(m_api != NULL && m_api->isOnlineApi())
    {
        connect(m_btnSearchGame, SIGNAL(clicked()), this, SLOT(onBtnSearchClicked()));
        connect(m_lineGameName, SIGNAL(textChanged(QString)), this, SLOT(onLineGameNameChanged(QString)));
        connect(m_chkDeleteGameLink, SIGNAL(clicked(bool)), this, SLOT(onClickCheckBoxDeleteLink(bool)));
    }
}

void WindowModifGame::paintEvent(QPaintEvent *pe)
{
    Q_UNUSED(pe);

    QPainter painter(this);
    QColor bgColor(255, 255, 255);
    QBrush brush(bgColor);
    painter.setPen(Qt::NoPen);
    painter.setBrush(brush);

    painter.drawRect(0, 0, this->width(), this->height());
}

void WindowModifGame::openExeFileSelector()
{
#ifdef WINDOWS
    QString path = QFileDialog::getOpenFileName(this, tr("Select an executable file"), QString(), tr("Executables (*.exe)"));
#else
    QString path = QFileDialog::getOpenFileName(this, tr("Select an executable file"), QString(), tr("Executables (*.*)"));
#endif
    if(path != "")
    {
        m_lineExePath->setText(path);
        m_lineExePath->setToolTip(path);
    }
}

void WindowModifGame::openUninstallFileSelector()
{
#ifdef WINDOWS
    QString path = QFileDialog::getOpenFileName(this, tr("Select the uninstall file"), QString(), tr("Executables (*.exe)"));
#else
    QString path = QFileDialog::getOpenFileName(this, tr("Select the uninstall file"), QString(), tr("Executables (*.*)"));
#endif

    if(path != "")
    {
        m_lineUninstallPath->setText(path);
        m_lineUninstallPath->setToolTip(path);
    }
}

void WindowModifGame::enableSaveSyncBox(bool on)
{
    m_lineSavePath->setText("");
    m_lineSavePath->setToolTip("");

    m_btnSavePathBrowse->setEnabled(on);
}

void WindowModifGame::openSavePathSelector()
{
    QString path = QFileDialog::getExistingDirectory(this, tr("Select save folder of the game"));

    if(path != "")
    {
        m_lineSavePath->setText(path);
        m_lineSavePath->setToolTip(path);
    }
}

void WindowModifGame::validateForm()
{
    //Verifying the emptiness of the fields
    if(m_lineGameName->text().isEmpty() || m_lineExePath->text().isEmpty())
    {
        QMessageBox::warning(this, tr("Warning"), tr("One of the compulsory fields is empty, please fullfill them"));
        return;
    }

    //Préparing variables
    QString gameName = m_lineGameName->text();
    QString gamePath = m_lineExePath->text();
    QString uninstallPath = m_lineUninstallPath->text();
    bool allowSaveSync = m_boxSaves->isChecked();
    QString savePath = "";


    if(m_boxSaves->isChecked())
    {
        //If the boxSave has been checked but no save path have been put into the lineEdit
        if(m_lineSavePath->text().isEmpty())
        {
            int response = QMessageBox::question(this, tr("Save Sync desactivation"), tr("You checked the save synchronisation box but didn't fill the <b>Game save folder</b> field.<br />Do you want to do this now ?<br />Clicking on No button will desactivate the save synchronisation"), QMessageBox::Yes | QMessageBox::No);

            if(response == QMessageBox::Yes)
            {
                openSavePathSelector();
                //If the text is still empty
                if(m_lineSavePath->text() == "")
                {
                    //Desactivation of the save sync
                    allowSaveSync = false;
                    m_boxSaves->setChecked(false);
                }
                else
                {
                    savePath = m_lineSavePath->text();
                }
            }
            else
            {
                allowSaveSync = false;
                m_boxSaves->setChecked(false);
            }
        }
        else
        {
            savePath = m_lineSavePath->text();
        }
    }

    //If gameId = 0, we are in the case of the addition of a new game
    if(m_gameId == 0)
    {
        if(m_game.add(gameName, gamePath, uninstallPath, savePath, allowSaveSync))
        {
            if(m_api && m_api->isOnlineApi())
                m_game.addApiGameAssociation(m_api->getApiName(), m_gameToLinkWith.getId());
            QMessageBox::information(this, tr("Success"), QString(gameName + " " + tr("has been successfully added")));
        }
    }
    else //Or else we want to update an existing game
    {
        if(m_game.update(m_gameId, gameName, gamePath, uninstallPath, savePath, allowSaveSync))
        {
            if(m_api && m_api->isOnlineApi())
            {
                if(m_chkDeleteGameLink->isChecked())
                {
                    m_game.deleteApiGameAssociation(m_api->getApiName(), m_gameId, m_currentRemoteGameId);
                }
                else
                {
                    m_game.updateApiGameAssociation(m_api->getApiName(), m_gameId, m_gameToLinkWith.getId(), m_currentRemoteGameId);
                }

            }
            QMessageBox::information(this, tr("Success"), QString(gameName + " " + tr("has been successfully updated")));
        }
    }
    this->accept();
}

void WindowModifGame::loadGameData()
{
    QString apiName = "basic";
    if(m_api && m_api->isOnlineApi())
        apiName = m_api->getApiName();

    QSqlRecord game = m_game.find(m_gameId, apiName);

    qDebug() << game.value("uninstallpath").toString() << endl;

    m_lineGameName->setText(game.value("name").toString());
    m_lineExePath->setText(game.value("exepath").toString());
    m_lineUninstallPath->setText(game.value("uninstallpath").toString());

    if(m_api && m_api->isOnlineApi())
    {
        m_gameToLinkWith.setId(game.value("remote_game_id").toInt());
        m_gameToLinkWith.setName(game.value("name").toString());
        m_currentRemoteGameId = game.value("remote_game_id").isNull() ? 0 : game.value("remote_game_id").toInt(0);

        if(!game.value("remote_game_id").isNull())
        {
            m_gameNameInfoLabel->setText("<font color='green'>" + tr("Linked with") + " <i>" + m_gameToLinkWith.getName() + "</i> " + tr("from remote database") + "</font>");
            m_gameNameInfoLabel->show();
        }
        else //If there is no current link to delete
        {
            m_chkDeleteGameLink->hide();
        }
    }

    m_boxSaves->setChecked(game.value("allowsavesync").toBool());
    if(m_boxSaves->isChecked())
    {
        enableSaveSyncBox(true);
        m_lineSavePath->setText(game.value("savepath").toString());
    }
}

void WindowModifGame::onBtnSearchClicked()
{
    connect(m_api, SIGNAL(finishedSearch(ApiGameList)), this, SLOT(onSearchFinished(ApiGameList)));
    m_btnSearchGame->setEnabled(false);
    m_btnSearchGame->setText(tr("Gathering data"));
    QString gameName = m_lineGameName->text();
    m_api->search(gameName);
}

void WindowModifGame::onSearchFinished(ApiGameList agl)
{    
    m_btnSearchGame->setText(tr("Search"));

    DialogSelectGame win(agl, &m_gameToLinkWith, this);
    win.exec();

    if((m_gameToLinkWith.getName() != m_lineGameName->text()) && !win.hasCancelledInput())
    {
        QString msg = tr("Your game name") + " (<i>" + m_lineGameName->text() + "</i>) " + tr("is different that the name which come from the database") + "(<i>" + m_gameToLinkWith.getName() + "</i>).<br />" + tr("Do you want to replace your name by the database name?");
        int rep = QMessageBox::question(this, tr("Game name replacement"), msg, QMessageBox::Yes | QMessageBox::No);

        if(rep == QMessageBox::Yes)
        {
            m_lineGameName->setText(m_gameToLinkWith.getName());
        }
    }

    if(!win.hasCancelledInput())
    {
        m_gameNameInfoLabel->setText("<font color='green'>" + tr("Linked with") + " <i>" + m_gameToLinkWith.getName() + "</i> " + tr("from remote database") + "</font>");
        m_gameNameInfoLabel->show();
    }

    m_api->disconnect(SIGNAL(finishedSearch(ApiGameList)));

    m_btnSearchGame->setEnabled(true);
}

void WindowModifGame::onLineGameNameChanged(QString text)
{
    if(!text.isEmpty())
    {
        m_btnSearchGame->setEnabled(true);
    }
    else
    {
        m_btnSearchGame->setEnabled(false);
    }
}

void WindowModifGame::onClickCheckBoxDeleteLink(bool on)
{
    m_btnSearchGame->setDisabled(on);
}
