#ifndef WINDOWMODIFGAME_H
#define WINDOWMODIFGAME_H

#include <iostream>
#include <QDialog>
#include <QVBoxLayout>
#include <QHBoxLayout>
#include <QFormLayout>
#include <QCheckBox>
#include <QLineEdit>
#include <QLabel>
#include <QPainter>
#include <QFileDialog>
#include <QMessageBox>
#include <QGroupBox>
#include <QSqlQuery>
#include <QSqlRecord>
#include "../apis/apis.h"
#include "../apis/apigame.h"
#include "games/db/games.h"
#include "../../gui/button.h"
#include "../gui/dialogselectgame.h"

class WindowModifGame : public QDialog
{
    Q_OBJECT
public:
    explicit WindowModifGame(QWidget *parent = 0, int gameId = 0);
    virtual ~WindowModifGame();
signals:
    
public slots:
    void openExeFileSelector();
    void openUninstallFileSelector();
    void openSavePathSelector();
    void enableSaveSyncBox(bool on);
    void validateForm();
    void onBtnSearchClicked();
    void onSearchFinished(ApiGameList agl);
    void onLineGameNameChanged(QString text);
    void onClickCheckBoxDeleteLink(bool on);
protected:
    void paintEvent(QPaintEvent *pe);

private:
    void buildGui();
    void initEvents();
    void loadGameData();

private:
    QVBoxLayout *m_mainLayout;
    QHBoxLayout *m_buttonsLayout;
    QHBoxLayout *m_exePathLayout;
    QHBoxLayout *m_uninstallPathLayout;
    QHBoxLayout *m_gameNameLayout;
    QHBoxLayout *m_savePathLayout;
    QFormLayout *m_formLayout;
    QFormLayout *m_savesFormLayout;

    QLineEdit *m_lineGameName;
    QLineEdit *m_lineExePath;
    QLineEdit *m_lineUninstallPath;
    QLineEdit *m_lineSavePath;

    QLabel *m_gameNameInfoLabel;

    QCheckBox *m_chkDeleteGameLink;

    QGroupBox *m_boxSaves;

    Button *m_btnValidate;
    Button *m_btnExePathBrowse;
    Button *m_btnUninstallPathBrowse;
    Button *m_btnSavePathBrowse;
    Button *m_btnCancel;
    Button *m_btnSearchGame; //Apis

    QString m_lastPositionExeWindow;
    QString m_lastPositionSaveWindow;
    
    Games m_game;

    AbstractApi *m_api;
    ApiGame m_gameToLinkWith;

    int m_gameId;
    int m_currentRemoteGameId; //Online APIs only
};

#endif // WINDOWMODIFGAME_H
